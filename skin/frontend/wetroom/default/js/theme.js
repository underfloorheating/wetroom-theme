var rbd = rbd || {}; // if namespace is not defined, make it equal to an empty object
jQuery(function($) {  // Pass jQuery as an argument to the anonymous function

    var bp = {
        xsmall: 479,
        small: 599,
        medium: 767,
        large: 979,
        xlarge: 1199
    };

    rbd.sF = {
        conf: {
            rw: '.wrapper',
            fl: '.form-list'
        },
        init: function(opts) {
            $.extend(rbd.sF.conf, opts);
            this.relExternal();
            this.cssAccord();
            this.navHover();
            this.misc();
            this.mediaCheck();
            this.plp();
            this.pdp();
            this.homepage();
        },
        relExternal: function() {
            //Valid target blank
            $('a[rel="external"]').attr('target', '_blank');
        },
        cssAccord: function() {
            //General CSS Accordion activation
            if ($.fn.css_accrd && $('dl.css_accrd').length) {
                $('dl.css_accrd').css_accrd();
            }
        },
        navHover: function() {
            if (!$.fn.hoverIntent)
                return;
            var $nav = $('#header-nav'),
                    dropDown = function() {
                        $(this).addClass('opened');
                    },
                    dropUp = function(e) {
                        $(this).removeClass('opened');
                    };
            $nav.hoverIntent({
                over: dropDown,
                out: dropUp,
                interval: 100,
                timeout: 200,
                selector: '.nav-primary > li.level0'
            });
            
            // Allow click through to underlying LI if we're on a touch device
            if ($j('html').hasClass('touch') && window.innerWidth > bp.medium) {
                $j('.nav-primary a.level0').css('pointer-events', 'none');
            }
            
        },
        mediaCheck: function() {
            enquire.register('(max-width: ' + (bp.medium) + 'px)', {
                match: function() {
                    var match = true;
                    $('html').addClass('sml');

                    $('.store-language-container').insertAfter('.logo');

                    if ($('.account-cart-wrapper').length) {
                        $('#header-cart').insertAfter('.account-cart-wrapper');
                    }

                    $('#select-language').insertAfter('.logo');

                    var $trophie = $('.hp-trophie').detach();
                    $('.hp-detail-blocks').prepend($trophie);

                    $('#header-account').insertAfter('.nav-primary .level0.last');


                },
                unmatch: function() {
                    var match = false;
                    $('html').removeClass('sml');

                    $('.store-language-container').insertBefore('.welcome-msg');
                    $('.hiring').insertAfter('ul.col-one');

                    if ($('.account-cart-wrapper').length) {
                        $('#select-language').insertAfter('.account-cart-wrapper .links');
                    }
                }
            });
        },
        misc: function() {
            // move language selector on page load
            if ($('.account-cart-wrapper').length) {
                $('#select-language').insertAfter('.account-cart-wrapper .links');
            }

            $('.mobile-home-banner').insertAfter('.nav-span');
            $('.desktop-home-banner').insertAfter('.nav-span');


            if ($('#bx-pager').length) {
                var pagers = jQuery('#bx-pager').find('a').length;
                jQuery('#bx-pager').find('a').width((100 / pagers) + '%');
            }

            if ($('.amount--no-pages').length) {
                $('.category-products .pager-container').addClass('no-pages');
            }


            $(window).load(function() {
                jQuery(this).find('.product-info').css({
                    'min-height': '0',
                    'padding-bottom': '0'
                });
            });

            if ($('.hp-full-width .column.hp-holder').length) {
                $('.cms-home .hp-full-width .column.hp-holder').css({'height': 'auto'}).equalHeights();
                $(window).on('delayed-resize', function() {
                    $('.cms-home .hp-full-width .column.hp-holder').css({'height': 'auto'}).equalHeights();
                });
            }
        },
        /**
         * Product listing page specific functions
         */
        plp: function() {
            $(window).load(function() {
                if ($('body').hasClass('catalog-category-view') || $('body').hasClass('catalogsearch-result-index')) {
                    /**
                     * Equalize grid elements height
                     */
                    $('.products-grid .item').css({'height': 'auto'}).equalHeights();
                    $('.products-grid .product-name').css({'height': 'auto'}).equalHeights();
                    $('.products-grid .price-box').css({'height': 'auto'}).equalHeights();
                    $(window).on('delayed-resize', function() {
                        $('.products-grid .item').css({'height': 'auto'}).equalHeights();
                        $('.products-grid .product-name').css({'height': 'auto'}).equalHeights();
                        $('.products-grid .price-box').css({'height': 'auto'}).equalHeights();
                    });

                    /**
                     * Check products for minimal price
                     */
                    $('.products-grid .item').each(function(index, el) {
                        if ($(el).find('a.minimal-price-link').length == 1) {
                            $(el).addClass('minimal-price');
                        }
                    });
                }
            });
        },
        /**
         * Product detail page specific functions
         */
        pdp: function() {
            $(window).load(function() {
                if ($('body').hasClass('catalog-product-view')) {
                    /**
                     * Equalize grid elements height
                     */
                    $('.box-related .mini-products-list .item').css({'height': 'auto'}).equalHeights();
                    $('.box-related .mini-products-list .item .product-name').css({'height': 'auto'}).equalHeights();
                    $(window).on('delayed-resize', function() {
                        $('.box-related .mini-products-list .item').css({'height': 'auto'}).equalHeights();
                        $('.box-related .mini-products-list .item .product-name').css({'height': 'auto'}).equalHeights();
                    });

                    /**
                     * Check the related products for minimal price
                     */
                    $('.mini-products-list li').each(function(index, el) {
                        if ($(el).find('a.minimal-price-link').length == 1) {
                            $(el).addClass('minimal-price');
                        }
                    });
                }
            });
        },
        /**
         * Home page specific functions
         */
        homepage: function() {
            $(document).ready(function(){
                //Hide imgs
                $('.hp-holder img').css('display','none');
                //Display elements
                $('.cms-home .top-cats').css('display', 'inline-block');
                //Set equal heights
                $('.cms-home .hp-full-width .column.hp-holder').css({'height': 'auto'}).equalHeights();
            });
            $(window).load(function() {
                //Show the imgs
                $('.hp-holder img').css('display','inline-block');
                //Re-equal the heights
                $('.cms-home .hp-full-width .column.hp-holder').css({'height': 'auto'}).equalHeights();
                $('.cms-home .hp-full-width .column.hp-holder .category-name').css({'height': 'auto'}).equalHeights();
                $('.catalog-product-view .box-related .mini-products-list .item').css({'height': 'auto'}).equalHeights();
                $('.catalog-product-view .box-related .mini-products-list .item .product-name').css({'height': 'auto'}).equalHeights();

            });
            $(window).on('delayed-resize', function() {
                $('.cms-home .hp-full-width .column.hp-holder').css({'height': 'auto'}).equalHeights();
                $('.cms-home .hp-full-width .column.hp-holder .category-name').css({'height': 'auto'}).equalHeights();
                $('.catalog-product-view .box-related .mini-products-list .item').css({'height': 'auto'}).equalHeights();
                $('.catalog-product-view .box-related .mini-products-list .item .product-name').css({'height': 'auto'}).equalHeights();
            });
        }
    };
// init
    rbd.sF.init();

});
