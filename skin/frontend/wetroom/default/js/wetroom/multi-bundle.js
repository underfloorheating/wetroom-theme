function display_neohub(statId) {
    $j('.product-options dt label').each(function () {
        if ($j(this).html() == "Do you require a hub?") {
            if ($j('select[name="bundle_option[' + statId + ']"] option:selected').text().indexOf('neo') >= 0) {
                $j(this).closest('dt').removeClass('disabled').next('dd').find('input').removeAttr('disabled');
            }
            else {
                $j(this).closest('dt').addClass('disabled').next('dd').find('input').attr({disabled:'disabled',checked:false});
                bundle.changeSelection($j(this).closest('dt').next('dd').find('input').get(0));
            }            
        }
    });  
}

$j(document).ready(function () {
    if (typeof bundle != "undefined") {
        var title = '',
        sizeId = 0,
        statId = 0;
        // get the first title in the 'Size' dropdown option box
        for (obj in bundle.config.options) {
            if (bundle.config.options[obj].title == "Size") {
                for (prop in bundle.config.options[obj].selections) {
                    if (prop > 0) {
                        sizeId = obj;
                        title = bundle.config.options[obj].selections[prop].name;
                        break;
                    }
                }
            }
            else if (bundle.config.options[obj].title == "Thermostat") {
                statId = obj;
            }
        }
        if (title.indexOf('Multiple Room') >= 0) {
            $j('select[name^="bundle_option"]').change(function () {
                var variant = $j('select[name="bundle_option[' + sizeId + ']"]').find(':selected').text();
                var reg = /(\d+) zone/g;
                var match = reg.exec(variant);
                var qty = parseInt(match[1]);
                $j('select[name="bundle_option[' + statId + ']"]').closest('dd').find('input[name="bundle_option_qty[' + statId + ']"]').val(qty).blur();

                // Code for applying quantity to thermostat option label
                $therm = $j('#bundle-option-'+statId);
                $therm.children().each(function(){
                    if($j(this).val() > 0){
                        if(typeof $j(this).data('orig') == "undefined"){
                            $j(this).data('orig',$j(this).html());
                        }
                        $j(this).html($j(this).data('orig')+" x "+qty);
                    }
                    
                });

            });
        }

        display_neohub(statId);

        // Undisable and disable hub option dependent on the thermostat selection
        $j('select[name^="bundle_option"]').change(function () {
            display_neohub(statId);
        });
    }

});
