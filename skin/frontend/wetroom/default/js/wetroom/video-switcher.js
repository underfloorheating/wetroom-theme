/*
 * Code has been moved to core.js
 */


// Load the Google SAPI scripting

var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

function onYouTubeIframeAPIReady() {

    player = new YT.Player('video-player', {
        height: '440',
        width: '718',
        videoId: 'Hcdt2XHqsgw',
        events: {
            'onReady': onPlayerReady
        }
    });

}

function onPlayerReady(event) {

    if (window.location.hash)
    {
        var hash = window.location.hash.substring(1);
        if ($j('li[rel="' + hash + '"]').length > 0)
        {
            var video_id = $j('li[rel="' + hash + '"] a:first-child').attr('rel');
            player.loadVideoById(video_id);
        }
        else
        {
            event.target.playVideo();
        }
    }
    else
    {
        event.target.playVideo();
    }
}

$j(document).ready(function() {
    
    $j('.video-thumbnails a').click(function() {
        
        $j('html, body').stop().animate({
            'scrollTop': $j('#video-player').offset().top - 50
        }, 500, 'swing');

        var video_id = $j(this).attr('rel');
        player.loadVideoById(video_id);
        
    });
    
});