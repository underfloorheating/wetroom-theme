// Breakpoint variables used throughout to determine what viewport we're on.
var bp = 'lg';

/**
 * Extend Number object to provide between functionality
 * 
 * @param INT a
 * @param INT b
 * @returns BOOLEAN
 */
 Number.prototype.between = function(a, b) {
    var min = Math.min.apply(Math, [a, b]);
    var max = Math.max.apply(Math, [a, b]);
    return this > min && this < max;
};

/**
 * Calculates the height of the biggest element in the group, then sets the 
 * min-height property on the rest of the elements in the group so they are 
 * always at the same height.
 * 
 * @param jQuery Object $group
 */
 var set_max_height = function($group) {

    $group.each(function(index, elem) {

        var selectors = $j(elem).data('height-determined-by').split(',');

        $j.each(selectors, function(index, selector) {

            // If we're in the mobile view, honour the skip indicator "|" and 
            // skip over the element
            if (bp == 'xs' || bp == 'mb') {
                if (selector.charAt(0) == '|') {
                    selector = selector.slice(1);
                    $j(selector).css('min-height', 0);
                    return true;
                }
            }

            // Clean up the selector string ready for use
            selector = selector.charAt(0) == '|' ? selector.slice(1) : selector;

            // Get the height of the biggest element in the group
            var maxHeight = Math.max.apply(null, $j(elem).find(selector).map(function() {

                // We have to wrap text node in span to allow height calculation
                if ($j(this).children().length == 0) {
                    $j(this).wrapInner('<span></span>');
                }

                totalHeight = 0;
                $j(this).children().each(function() {
                    totalHeight += $j(this).outerHeight(true);
                });
                return totalHeight;

            }).get());


            // Set the min-height value on all elements in the group
            $j(elem).find(selector).css('height', maxHeight);

        });
    });
}

/**
 * Set the global bp variable to the represent the current media query breakpoint.
 */
 var set_breakpoint = function() {

    var xsmall = 479;
    var small = 599;
    var medium = 770;
    var large = 959;
    var xlarge = 1199;

    var winWidth = $j(window).width();

    if (winWidth < xsmall) {
        bp = 'mb';
    }
    else if (winWidth.between(xsmall, small + 1)) {
        bp = 'xs';
    }
    else if (winWidth.between(small, medium + 1)) {
        bp = 'sm';
    }
    else if (winWidth.between(medium, large + 1)) {
        bp = 'md';
    }
    else if (winWidth.between(large, xlarge + 1)) {
        bp = 'lg';
    }
    else {
        bp = 'xl';
    }
};

var activate_tab = function(tab) {
    if ($j('.toggle-tabs li:contains(' + tab + ')').length > 0) {
        $j('.toggle-tabs li:contains(' + tab + ')').trigger('click');
    }
}

var return_last_uri_segment = function(val) {
    return val.substr(val.lastIndexOf('/') + 1);
}
/**
 * Return Simple Product ID Under Configurable Product
 */
 var prepare_spConfig = function() {
    spConfig.getIdOfSelectedProduct = function() {
        var existingProducts = new Object();
        for (var i = this.settings.length - 1; i >= 0; i--) {
            var selected = this.settings[i].options[this.settings[i].selectedIndex];
            if (selected.config) {
                for (var iproducts = 0; iproducts < selected.config.products.length; iproducts++) {
                    var usedAsKey = selected.config.products[iproducts] + "";
                    if (existingProducts[usedAsKey] == undefined) {
                        existingProducts[usedAsKey] = 1;
                    } else {
                        existingProducts[usedAsKey] = existingProducts[usedAsKey] + 1;
                    }
                }
            }
        }
        for (var keyValue in existingProducts) {
            for (var keyValueInner in existingProducts) {
                if (Number(existingProducts[keyValueInner]) < Number(existingProducts[keyValue])) {
                    delete existingProducts[keyValueInner];
                }
            }
        }
        var sizeOfExistingProducts = 0;
        var currentSimpleProductId = "";
        for (var keyValue in existingProducts) {
            currentSimpleProductId = keyValue;
            sizeOfExistingProducts = sizeOfExistingProducts + 1
        }
        if (sizeOfExistingProducts == 1) {  
            return currentSimpleProductId;	
        }
    }
}

var display_minimum_qty = function (min){
    if($j('#minimum-qty').length < 1){
        $j('.product-options-bottom .price-box').append('<span id="minimum-qty">Minimum purchase is '+min+' boards.</span>');
    }
    else {
        $j('#minimum-qty').html('Minimum purchase is '+min+' boards.');
    }
}



$j(window).load(function() {
    set_breakpoint();

    // Manage the height of any height managed panels
    if ($j('[data-height-determined-by]').length > 0) {
        set_max_height($j('[data-height-determined-by]'));
    }

});

$j(window).resize(function() {

    set_breakpoint();

    // Manage the height of any height managed panels
    if ($j('[data-height-determined-by]').length > 0) {
        set_max_height($j('[data-height-determined-by]'));
    }

});

$j(document).ready(function() {   

    // Functionality for the hp-error-notice
    $j('#overlay-close').click(function(){
        $j('#error-overlay').addClass('hidden');
    });

    // Force a window resize event if any of the product view tabs are opened (this is because on DOM ready the tab description elements are width:auto;)
    $j('#collateral-tabs dt').click(function(){
        $j(window).trigger('resize');
    });

    if($j('#lustroliteColours').length > 0){
        var size = $j(window).width() < 480 ? 180 : 250;

        $j('#lustroliteColours').kwicks({
            maxSize : size,
            spacing : 0,
            behavior: 'menu'
        });
    }
    

    // Inject content into 'Do you require a hub?'
    $j('.product-options dt label').each(function(){
        if($j(this).html() == "Do you require a hub?"){
            $j(this).closest('dt').next('dd').append('<span class="require-hub">view hub details</span>');
        }
    });
    
    // Added clicking effect to view hub details
    $j('.require-hub').click(function(){
        activate_tab('Thermostat'); 
        $j('html,body').animate({scrollTop: $j('a[name="neohub"]').offset().top},1000);
    });

    // Added clicking effect to view hub details
    $j('.delivery-tab-trigger').click(function(){
        activate_tab('Delivery'); 
        $j('html,body').animate({scrollTop: $j('.tabs').offset().top},1000);
    });
    
    // On clicking Add to basket
    $j('.btn-cart').click(function(){
        if($j('.validation-advice').length <=0 || $j('.validation-advice').css('opacity') != "1"){
            $j(this).children('span').first().html('<span>Adding to basket</span>');
            $j(this).addClass('loading');
        }
        
    });

    // Minimum QTYs for configurable products with minimum QTY simple products 
        // If spConfig is defined, e.g. it's a configurable product
        if(typeof spConfig != "undefined"){
        // Prepare the spConfig function that returns the current simple product's id
        prepare_spConfig();
        // If any of the product options select boxs are changed
        $j('.product-options select').change(function(){
            // Generate the new simple product id
            simpleId = spConfig.getIdOfSelectedProduct();
            // If a minimum qty exists
            if (simpleqtys[simpleId] > 1){
                // Display the minimum qty message
                display_minimum_qty(simpleqtys[simpleId]);
            }
            // If a rrp exists
            if (typeof configrrps[simpleId] != "undefined"){
                // Inject the new RRP
                if(configrrps[simpleId].length > 1){
                    $j('p.msrp-rrp span').html(configrrps[simpleId]);
                }                
            }
            // If the value of the qty field is less than the minimum qty allowed for that simple product 
            if ($j('input#qty').val() < simpleqtys[simpleId]){                
                // Set the qty field to be the minimum qty allowed
                $j('input#qty').val(simpleqtys[simpleId]);                
            }
        });
        // If the qty field is updated
        $j('input#qty').change(function(){
            // Generate the current simple product id
            simpleId = spConfig.getIdOfSelectedProduct();
            // If the value of the qty field is less than the minimum qty allowed for that simple product
            if ($j(this).val() < simpleqtys[simpleId]){
                // Set the qty field to be the minimum qty allowed
                $j(this).val(simpleqtys[simpleId]);   
                // And display the minimum qty message
                display_minimum_qty(simpleqtys[simpleId]);
            }
        });
        
        
        if(typeof defaultVals != "undefined"){          
            selects = $j('select.super-attribute-select');
            selects.each(function(index,elem){
                $j(this).children('option').eq(1).prop('selected', true);   
                if(index == selects.length - 1){
                    setTimeout(function(){  
                        //Declared event createEvent way for IE compatibility
                        var event = document.createEvent('Event');
                        event.initEvent('change',true,true);
                        elem.dispatchEvent(event);
                    },750); 
                }                
            });         
        }
    }

    if(typeof bundle != "undefined"){
        var $bundleDropdowns = $j('select[name^="bundle_option"], input[name^="bundle_option"][type="checkbox"]');

        if(typeof bundlerrps != "undefined"){
            $bundleDropdowns.change(function(){
                var rrp = 0;
                $j.each($bundleDropdowns, function(i, dropDown) {
                    $dropDown = $j(dropDown);
                    var bundleId = /.*?\[(.*?)\]/.exec($dropDown.attr('name'))[1];
                    var selectedId = $dropDown.prop('tagName') == "INPUT" && $dropDown.is(":checked") ? $dropDown.val() : $dropDown.find(':selected').val();
                    rrp += bundlerrps[bundleId][selectedId] ? bundlerrps[bundleId][selectedId] : 0;
                });
                if(rrp > 0){
                    $j('p.msrp-rrp').removeClass('hidden').children('span').html(bundlerrps['symbol'] + parseFloat(rrp).toFixed(2));
                }
                else {
                    $j('p.msrp-rrp').addClass('hidden');
                }  
            }).trigger('change');            
        }
        if(typeof bundlelist != "undefined"){
            $bundleDropdowns.change(function(){
                var list = 0;
                $j.each($bundleDropdowns, function(i, dropDown) {
                    $dropDown = $j(dropDown);
                    var bundleId = /.*?\[(.*?)\]/.exec($dropDown.attr('name'))[1];
                    var selectedId = $dropDown.prop('tagName') == "INPUT" && $dropDown.is(":checked") ? $dropDown.val() : $dropDown.find(':selected').val();
                    list += bundlelist[bundleId][selectedId] ? bundlelist[bundleId][selectedId] : 0;
                });
                if(list > 0){
                    $j('.product-list-price').removeClass('hidden').children('span').html(bundlelist['symbol'] + parseFloat(list).toFixed(2));
                }
                else {
                    $j('.product-list-price').addClass('hidden');
                }  
            }).trigger('change');     
        }
    }

    // Loading Overlay Button
    $j('.loading-overlay-button').click(function(){
        setTimeout(function(){
            $j('#loading-overlay').addClass('active');
        },2000);
    });

    //---------------------------//
    // Flyout Menu functionality //
    //---------------------------//

    // Core functionality of the flyout menu image swapper
        // When you hover over a flyout item
        $j('li.level1').on('mouseover touchend', function() {
        // Set flyout to equal all of the flyout menu images
        flyout = $j(this).parent().siblings('.cat_sb').children('.flyout-menu-image');
        // And set the current flyout menu image
        current = $j(this).parent().siblings('.cat_sb').children('.flyout-menu-image[data-flyout-image="' + return_last_uri_segment($j(this).children('a').attr('href')) + '"]');
        // If there current one exists
        if (current.length > 0) {
            // Hide the others
            flyout.addClass('hidden');
            // And show the current
            current.removeClass('hidden');
        }
        else {
            // Otherwise hide all the flyout menu images
            flyout.addClass('hidden');
            // And show the default
            $j('.flyout-menu-image.default').removeClass('hidden');
        }
    });   

    //Ensures there is always a flyout image visible by checking to make sure that all of them aren't hidden
        // When you mouseouse the flyout menu item holder
        $j('.li-holder').mouseout(function(){
        // Set check to be true
        check = 1;
        // Iterate over all flyout menu images
        $j(this).siblings('.cat_sb').children().each(function(){
            // If any of them are visible
            if(!$j(this).hasClass('hidden')){
                // Set check to be false
                check = 0;
            }
        });
        // If check is still true after the iteration, e.g all the other flyout menu images are hidden
        if(check == 1){
            // Show the default flyout menu image
            $j('.flyout-menu-image.default').removeClass('hidden');
        }        
    });   

    // Allows the user to hover onto a flyout menu image, allowing for a sub-navigation
        // When you hover onto a flyout menu image
        $j('.flyout-menu-image').hover(function(){
        // All other flyout menu image within that category are hidden
        $j(this).siblings().addClass('hidden');
        // And this one is shown
        $j(this).removeClass('hidden');
    });   

    // Resets the flyout menu image on hover out of the flyout menu
        // When you hover onto the flyout menu
        $j('ul.level0').hover(
        // Do nothing
        function(){},
        // However on hover out
        function(){
            // Hide all flyout menu images
            $j('.flyout-menu-image').addClass('hidden');
            // And show the default
            $j('.flyout-menu-image.default').removeClass('hidden');
        }
        );

    // Scroll for comparison a tag link
    $j('a.scroll-link').click(function(){
        var anchor = $j(this).attr('href').substr(1);
        $j('html,body').animate({scrollTop: $j('a[name="'+anchor+'"]').offset().top},1000);
    });
    
    
    

    /**
     * Custom Comm100 live chat button handling
     */
     $j('.comm-button').click(function() {
        Comm100API.open_chat_window(event, 511);
    });

    /**
     * Social media sharing for videos and manuals
     */
     $j('.sharing a').on('click', function() {
        $self = $j(this);
        var cls = $self.attr('class');

        // Get handle to associated video
        var $item = $self.closest('div').siblings('a').eq(0);

        // Grab URL
        var href = $item.attr('href');

        // Ensure link is absolute
        var shareUrl = href.match(/http/g) ? href : 'http://' + window.location.host + href;

        // Grab associated image
        var imageUrl = $item.find('img').eq(0).attr('src');
        var imageUrl = imageUrl.match(/http/g) ? href : 'http://' + window.location.host + imageUrl;

        // Get the display title
        var itemDescription = $self.closest('div').siblings('p').eq(0).text();

        // Handle Facebook sharing
        if (cls == 'facebook') {
            FB.ui({
                method: 'feed',
                link: shareUrl,
                name: $item.attr('title'),
                description: itemDescription,
                picture: imageUrl,
            }, function(response) {
            });
        }

        if (cls == 'twitter') {
            window.open('https://twitter.com/intent/tweet?text=' + encodeURI(itemDescription) + '&url=' + shareUrl + '&via=underfloorhs', '_blank', 'height=530,width=560,left=50,top=50,location=0,menubar=0,resizable=0,scrollbars=0,status=0,titlebar=0,toolbar=0');
        }
    });

    /**
     * Product Guarantee accordion on terms and conditions page
     */
     $j('#product-guarantees h2').click(function() {
        $j(this).toggleClass('open');
        $j('#product-guarantees h2').not($j(this)).removeClass('open');
        $elem = $j(this);
        setTimeout(function() {
            $j('body').animate({
                scrollTop: $elem.offset().top
            }, 1000);
        }, 500);

    });

    /**
     * Handles the control of the Jobs Accordion
     */
     $j('#jobs-accordion dt').click(function() {
        if ($j(this).hasClass('active')) {
            $j(this).removeClass('active');
        }
        else {
            $j('#jobs-accordion dt').removeClass('active');
            $j(this).addClass('active');
        }
    });

    /**
     * Fade footer 'As Seen On' strips
     */
     setInterval(function() {
        $j('#as-seen-in-container ul').toggleClass('active');
    }, 5000);

     $j('#help-selector').change(function() {
        window.location = "/" + $j(this).val();
    });

    /**
     * Adds column and row highlighting to table cells on hover
     */
     $j('table.interactive td').hover(
        function() {
            var $self = $j(this);
            var column = $self.index();
            column++;
            $self.closest('table').find('td:nth-of-type(' + column + ')').not($self).addClass('focus');
            $self.closest('tr').find('td').not($self).addClass('focus');
        },
        function() {
            $j('td').removeClass('focus');
        });

    /**
     * Dynamically cleanup youtube URL to include required attributes
     */

     $j('iframe[src*="youtube"], a[href*="youtube"]').each(function() {
        var $self = $j(this);
        var source = $self.get(0).tagName == 'IFRAME' ? 'src' : 'href';
        var sourceValue = $self.attr(source);
        var sourceValue = sourceValue.indexOf('?') != -1 ? sourceValue.substring(0, sourceValue.indexOf('?')) : sourceValue;
        $self.attr(source, sourceValue + '?fs=0&modestbranding=1&rel=0&showinfo=0');
    });

 });
