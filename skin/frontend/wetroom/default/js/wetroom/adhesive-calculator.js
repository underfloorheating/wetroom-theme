var adhesiveCal = adhesiveCal || {};

(function($){

    adhesiveCal = {
        handles: {},
        config: {
            calculator: '',
            areaInput: '.area',
            depthInput: '.thickness',
            calcButton: '.calculate',
            output: '.output',
            addButton: '.add-to-cart',
            qtyHolder: 'input[type="hidden"]',
            coverage: 0,
            label: 'bags',
            size: 0,
            mageQty: '.qty-wrapper input'
        },
        inputChange: function(){
            handles.output.addClass('invisible');
            handles.addButton.addClass('invisible');
        },
        validateInput: function(elem){
            return (isNaN(elem.val()) || elem.val() <= 0 || elem.val() > parseInt(elem.attr('max'))) ? false : true;
        },
        calculate: function(){
            handles.output.removeClass('invisible');
            if(validate(handles.areaInput) && validate(handles.depthInput)){  
                handles.addButton.removeClass('invisible'); 
                area = parseInt(handles.areaInput.val());
                coverage = handles.coverage;
                depth = parseInt(handles.depthInput.val());
                size = handles.size;
                handles.qtyHolder.val(Math.ceil((area*coverage*depth)/size));
                result = Math.ceil((area*coverage*depth)/size) + " " + handles.label;
                handles.output.children('span').html(result);             
            }
            else {
                handles.output.children('span').html('Please enter a valid number');                
            }
        },
        addCart: function(){
            handles.mageQty.val(handles.qtyHolder.val());
            window.productAddToCartForm.submit(this);
        },
        init: function(config){
            $.extend(this.config,config);
            calculator = $('#'+config.calculator);
            if(calculator.length > 0){
                this.handles.mageQty = $(this.config.mageQty);
                this.handles.calcButton = calculator.find(this.config.calcButton);
                this.handles.output = calculator.find(this.config.output);
                this.handles.addButton = calculator.find(this.config.addButton);
                this.handles.qtyHolder = calculator.find(this.config.qtyHolder);
                this.handles.coverage = this.config.coverage;
                this.handles.label = this.config.label;
                this.handles.size = this.config.size;
                this.handles.areaInput = calculator.find(this.config.areaInput).children('input');
                this.handles.depthInput = calculator.find(this.config.depthInput).children('input');
            }
            else {
                console.log('Failed to find calculator element.');
                return false;
            }
            config = this.config;
            handles = this.handles;
            validate = this.validateInput;
            handles.calcButton.bind("click",this.calculate);
            handles.addButton.bind('click',this.addCart);
            handles.areaInput.bind('change',this.inputChange);
            handles.depthInput.bind('change',this.inputChange);
        }
    };

})(jQuery)
