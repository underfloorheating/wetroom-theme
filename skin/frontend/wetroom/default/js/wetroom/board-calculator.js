var boardCal = boardCal || {};

(function($){

    boardCal = {    
        handles: {},    
        config: {
            calculator: '',
            input: '',
            calcButton: '.calculate',
            output: '.output',
            addButton: '.add-to-cart',
            qtyHolder: 'input[type="hidden"]',
            label: 'boards',
            coverage: 0,
            mageQty: '.qty-wrapper input'
        },
        validateInput: function(elem){
            return (isNaN(elem.val()) || elem.val() <= 0 || elem.val() > parseInt(elem.attr('max'))) ? false : true;
        },
        inputChange: function(){
            handles.output.addClass('invisible');
            handles.addButton.addClass('invisible');
        },
        addCart: function(){
            handles.mageQty.val(handles.qtyHolder.val());
            window.productAddToCartForm.submit(this);
        },
        calculate: function(){
            if(handles.input.val() < parseInt(handles.input.attr('min'))){
                return true;
            }
            handles.output.removeClass('invisible');
            if(validate(handles.input)){
                handles.addButton.removeClass('invisible');
                qty = parseInt(handles.input.val());
                max = parseInt(handles.input.attr('max'));
                handles.qtyHolder.val(Math.ceil(qty / handles.coverage));
                result = (isNaN(qty) || qty <= 0 || qty > max) ? 'Please enter a valid number' : Math.ceil(qty / handles.coverage) + " " + handles.label;
                handles.output.children('span').html(result);
            }
            else {
                handles.output.children('span').html('Please enter a valid number');
            }            
        },
        simpleChange: function(){
            handles.input.attr('min',Math.floor(boardCal.config.simples[window.spConfig.getIdOfSelectedProduct()] * boardCal.config.coverage));
            handles.output.addClass('invisible');
            handles.addButton.addClass('invisible');
        },
        init: function(config){
            $.extend(this.config, config);
            calculator = $('#'+config.calculator);
            if(calculator.length > 0){
                this.handles.mageQty = $(this.config.mageQty);
                this.handles.input = calculator.children('.input').children('input');
                this.handles.calcButton = calculator.find(this.config.calcButton);
                this.handles.output = calculator.find(this.config.output);
                this.handles.addButton = calculator.find(this.config.addButton);
                this.handles.qtyHolder = calculator.find(this.config.qtyHolder);
                this.handles.coverage = this.config.coverage;
                this.handles.label = this.config.label;
            }
            else {
                console.log('Failed to find calculator element.');
                return false;
            }
            config = this.config;
            handles = this.handles;
            validate = this.validateInput;
            handles.calcButton.bind("click",this.calculate);
            handles.input.bind('change',this.inputChange);
            handles.addButton.bind('click',this.addCart);
            if(typeof this.config.simples != "undefined"){
                try {
                    window.spConfig.getIdOfSelectedProduct;
                    this.config.simpleTrig.bind('change',this.simpleChange);
                }
                catch (err){
                    return false;
                }
            }            
        }
    };

})(jQuery)
