
$j(document).ready(function () {
    $j('#cableCal-iframe input').blur(function () {
        cableCal_on_blur($j(this));
    });

    $j('.sep.add').click(function () {
        cableCal_add_new();
    });
});

//On Input Blur
var cableCal_on_blur = function (elem) {
    //Get the length and width from that form
    var len = elem.closest('.form').children('.length').children('input').val();
    var wdh = elem.closest('.form').children('.width').children('input').val();
    //Make the total in that from equal to the length * width
    elem.closest('.form').children('.total').children('input').val((len * wdh).toFixed(2));

    //Step-two result equals zero
    var stepTwoResult = 0.00;
    //If an input in step two has been clicked
    if (elem.closest('form').hasClass('step-two')) {
        //Then for each entry
        $j('form.step-two > div.form').each(function () {
            //Parse the value of the entries total into a parse and plus it to Step-two result
            stepTwoResult += parseFloat($j(this).children('.total').find('input').val());
        });
        //Finally pass the total value to the hidden total for step two
        $j('form.step-two > .total > input[type="hidden"]').val((stepTwoResult).toFixed(2));
    }

    //Work out the total room space
    $j('form.step-three').find('.space').children('input').val(parseFloat($j('form.step-one').find('.total').children('input').val()));
    //Work out the total footprint
    $j('form.step-three').find('.fixed').children('input').val(parseFloat($j('form.step-two').find('.total').children('input').val()));
    //Then minus the footprint from the total room space
    $j('form.step-three').find('.total').children('input').val(($j('form.step-three').find('.space').children('input').val() - $j('form.step-three').find('.fixed').children('input').val()).toFixed(2));
    //And minus ten percentage and convert it to a string with no decimal places
    var final = (parseFloat($j('form.step-three').find('.total').children('input').val()) * 0.9).toFixed(0);
    //Finally tag m2 on the end on place it in the final fitted area input
    $j('.input-parent.final').children('input').val(final + "m2");
}

//On add new entry
var cableCal_add_new = function () {
    //Define what an entry is and append it to step two form
    var entry = $j('<div class="form"><div class="input-parent length"><label>Length (m)</label><input type="number" min="0"/></div><span class="sep times">x</span><div class="input-parent width"><label>Width (m)</label><input type="number" min="0"/></div><div class="input-parent total hidden"><input type="hidden" min="0" value="0.00" disabled/></div></div>').appendTo('form.step-two');

    //Define what happens on blur of an input of a newly made entry
    $j('input', entry).blur(function () {
        cableCal_on_blur($j(this));
    });
}
