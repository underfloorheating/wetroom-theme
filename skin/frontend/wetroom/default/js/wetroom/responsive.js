/*  ==|== Responsive =============================================================
    Author: James South
    twitter : http://twitter.com/James_M_South
    github : https://github.com/ResponsiveBP/Responsive
    Copyright (c),  James South.
    Licensed under the MIT License.
    ============================================================================== */

/*! Responsive v4.1.1 | MIT License | responsivebp.com */

/*
 * Responsive Core
 */

/*global jQuery*/
/*jshint forin:false, expr:true*/
(function ($, w, d) {

    "use strict";

    $.pseudoUnique = function (length) {
        /// <summary>Returns a pseudo unique alpha-numeric string of the given length.</summary>
        /// <param name="length" type="Number">The length of the string to return. Defaults to 8.</param>
        /// <returns type="String">The pseudo unique alpha-numeric string.</returns>

        var len = length || 8,
            text = "",
            possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789",
            max = possible.length;

        if (len > max) {
            len = max;
        }

        for (var i = 0; i < len; i += 1) {
            text += possible.charAt(Math.floor(Math.random() * max));
        }

        return text;
    };

    $.support.rtl = (function () {
        /// <summary>Returns a value indicating whether the current page is setup for right-to-left languages.</summary>
        /// <returns type="Boolean">
        ///      True if right-to-left language support is set up; otherwise false.
        ///</returns>

        return $("html[dir=rtl]").length ? true : false;
    }());

    $.support.currentGrid = (function () {
        /// <summary>Returns a value indicating what grid range the current browser width is within.</summary>
        /// <returns type="Object">
        ///   An object containing two properties.
        ///   &#10;    1: grid - The current applied grid; either xxs, xs, s, m, or l.
        ///   &#10;    2: index - The index of the current grid in the range.
        ///   &#10;    3: range - The available grid range.
        ///</returns>
        return function () {
            var $div = $("<div/>").addClass("grid-state-indicator").prependTo("body");

            // These numbers match values in the css
            var grids = ["xxs", "xs", "s", "m", "l"],
                key = parseInt($div.width(), 10);

            $div.remove();

            return {
                grid: grids[key],
                index: key,
                range: grids
            };
        };
    }());

    $.support.scrollbarWidth = (function () {
        /// <summary>Returns a value indicating the width of the browser scrollbar.</summary>
        /// <returns type="Number">The width in pixels.</returns>
        return function () {

            var width = 0;
            if (d.body.clientWidth < w.innerWidth) {

                var $div = $("<div/>").addClass("scrollbar-measure").prependTo("body");
                width = $div[0].offsetWidth - $div[0].clientWidth;

                $div.remove();
            }

            return width;
        };
    }());

    $.toggleBodyLock = function () {
        /// <summary>
        /// Toggles a locked state on the body which toggles both scrollbar visibility and padding on the body.
        /// </summary>

        var $html = $("html"),
            $body = $("body"),
            bodyPad;

        // Remove.
        if ($html.attr("data-lock") !== undefined) {

            bodyPad = $body.data("bodyPad");
            $body.css("padding-right", bodyPad || "")
                 .removeData("bodyPad");

            $html.removeAttr("data-lock")
                 .trigger($.Event("unlock.r.bodylock"));
            return;
        }

        // Add
        bodyPad = parseInt($body.css("padding-right") || 0);
        var scrollWidth = $.support.scrollbarWidth();

        if (scrollWidth) {
            $body.css("padding-right", bodyPad + scrollWidth);

            if (bodyPad) {
                $body.data("bodyPad", bodyPad);
            }

            $html.attr("data-lock", "")
                 .trigger($.Event("lock.r.bodylock", { padding: bodyPad + scrollWidth }));
        }
    };

    $.support.transition = (function () {
        /// <summary>Returns a value indicating whether the browser supports CSS transitions.</summary>
        /// <returns type="Boolean">True if the current browser supports css transitions.</returns>

        var transitionEnd = function () {
            /// <summary>Gets transition end event for the current browser.</summary>
            /// <returns type="Object">The transition end event for the current browser.</returns>

            var div = d.createElement("div"),
                transEndEventNames = {
                    "transition": "transitionend",
                    "WebkitTransition": "webkitTransitionEnd",
                    "MozTransition": "transitionend",
                    "OTransition": "oTransitionEnd otransitionend"
                };

            var names = Object.keys(transEndEventNames),
                len = names.length;

            for (var i = 0; i < len; i++) {
                if (div.style[names[i]] !== undefined) {
                    return { end: transEndEventNames[names[i]] };
                }
            }

            return false;
        };

        return transitionEnd();

    }());

    $.fn.redraw = function () {
        /// <summary>Forces the browser to redraw by measuring the given target.</summary>
        /// <returns type="jQuery">The jQuery object for chaining.</returns>
        var redraw;
        return this.each(function () {
            redraw = this.offsetWidth;
        });
    };

    (function () {
        var getDuration = function ($element) {
            var rtransition = /\d+(.\d+)?/;
            return (rtransition.test($element.css("transition-duration")) ? $element.css("transition-duration").match(rtransition)[0] : 0) * 1000;
        };

        $.fn.ensureTransitionEnd = function (duration) {
            /// <summary>
            /// Ensures that the transition end callback is triggered.
            /// http://blog.alexmaccaw.com/css-transitions
            ///</summary>

            if (!$.support.transition) {
                return this;
            }

            var called = false,
                $this = $(this),
                callback = function () { if (!called) { $this.trigger($.support.transition.end); } };

            if (!duration) {
                duration = getDuration($this);
            }

            $this.one($.support.transition.end, function () { called = true; });
            w.setTimeout(callback, duration);
            return this;
        };

        $.fn.onTransitionEnd = function (callback) {
            /// <summary>Performs the given callback at the end of a css transition.</summary>
            /// <param name="callback" type="Function">The function to call on transition end.</param>
            /// <returns type="jQuery">The jQuery object for chaining.</returns>

            var supportTransition = $.support.transition;
            return this.each(function () {

                if (!$.isFunction(callback)) {
                    return;
                }

                var $this = $(this),
                    duration = getDuration($this),
                    error = duration / 10,
                    start = new Date(),
                    args = arguments;

                $this.redraw();
                supportTransition ? $this.one(supportTransition.end, function () {
                    // Prevent events firing too early.
                    var end = new Date();
                    if (end.getMilliseconds() - start.getMilliseconds() <= error) {
                        w.setTimeout(function () {
                            callback.apply(this, args);
                        }.bind(this), duration);
                        return;
                    }

                    callback.apply(this, args);

                }) : callback.apply(this, args);
            });
        };
    }());

    $.support.touchEvents = (function () {
        return ("ontouchstart" in w) || (w.DocumentTouch && d instanceof w.DocumentTouch);
    }());

    $.support.pointerEvents = (function () {
        return (w.PointerEvent || w.MSPointerEvent);
    }());

    (function () {
        var supportTouch = $.support.touchEvents,
            supportPointer = $.support.pointerEvents;

        var pointerStart = ["pointerdown", "MSPointerDown"],
            pointerMove = ["pointermove", "MSPointerMove"],
            pointerEnd = ["pointerup", "pointerout", "pointercancel", "pointerleave",
                          "MSPointerUp", "MSPointerOut", "MSPointerCancel", "MSPointerLeave"];

        var touchStart = "touchstart",
            touchMove = "touchmove",
            touchEnd = ["touchend", "touchleave", "touchcancel"];

        var mouseStart = "mousedown",
            mouseMove = "mousemove",
            mouseEnd = ["mouseup", "mouseleave"];

        var getEvents = function (ns) {
            var estart,
                emove,
                eend;

            // Keep the events separate since support could be crazy.
            if (supportTouch) {
                estart = touchStart + ns;
                emove = touchMove + ns;
                eend = (touchEnd.join(ns + " ")) + ns;
            }
            else if (supportPointer) {
                estart = (pointerStart.join(ns + " ")) + ns;
                emove = (pointerMove.join(ns + " ")) + ns;
                eend = (pointerEnd.join(ns + " ")) + ns;

            } else {
                estart = mouseStart + ns;
                emove = mouseMove + ns;
                eend = (mouseEnd.join(ns + " ")) + ns;
            }

            return {
                start: estart,
                move: emove,
                end: eend
            };
        };

        var addSwipe = function ($elem, handler) {
            /// <summary>Adds swiping functionality to the given element.</summary>
            /// <param name="$elem" type="Object">
            ///      The jQuery object representing the given node(s).
            /// </param>
            /// <returns type="jQuery">The jQuery object for chaining.</returns>

            var ns = handler.namespace ? "." + handler.namespace : "",
                eswipestart = "swipestart",
                eswipemove = "swipemove",
                eswipeend = "swipeend",
                etouch = getEvents(ns);

            // Set the touchAction variable for move.
            var touchAction = handler.data && handler.data.touchAction || "none",
                sensitivity = handler.data && handler.data.sensitivity || 5;

            if (supportPointer) {
                // Enable extended touch events on supported browsers before any touch events.
                $elem.css({ "-ms-touch-action": "" + touchAction + "", "touch-action": "" + touchAction + "" });
            }

            return $elem.each(function () {
                var $this = $(this);

                var start = {},
                    delta = {},
                    onMove = function (event) {

                        // Normalize the variables.
                        var isMouse = event.type === "mousemove",
                            isPointer = event.type !== "touchmove" && !isMouse,
                            original = event.originalEvent,
                            moveEvent;

                        // Only left click allowed.
                        if (isMouse && event.which !== 1) {
                            return;
                        }

                        // One touch allowed.
                        if (original.touches && original.touches.length > 1) {
                            return;
                        }

                        // Ensure swiping with one touch and not pinching.
                        if (event.scale && event.scale !== 1) {
                            return;
                        }

                        var dx = (isMouse ? original.pageX : isPointer ? original.clientX : original.touches[0].pageX) - start.x,
                            dy = (isMouse ? original.pageY : isPointer ? original.clientY : original.touches[0].pageY) - start.y;

                        var doSwipe,
                            percentX = Math.abs(parseFloat((dx / $this.width()) * 100)) || 100,
                            percentY = Math.abs(parseFloat((dy / $this.height()) * 100)) || 100;

                        // Work out whether to do a scroll based on the sensitivity limit.
                        switch (touchAction) {
                            case "pan-x":
                                if (Math.abs(dy) > Math.abs(dx)) {
                                    event.preventDefault();
                                }
                                doSwipe = Math.abs(dy) > Math.abs(dx) && Math.abs(dy) > sensitivity && percentY < 100;
                                break;
                            case "pan-y":
                                if (Math.abs(dx) > Math.abs(dy)) {
                                    event.preventDefault();
                                }
                                doSwipe = Math.abs(dx) > Math.abs(dy) && Math.abs(dx) > sensitivity && percentX < 100;
                                break;
                            default:
                                event.preventDefault();
                                doSwipe = Math.abs(dy) > sensitivity || Math.abs(dx) > sensitivity && percentX < 100 && percentY < 100;
                                break;
                        }

                        event.stopPropagation();

                        if (!doSwipe) {
                            return;
                        }

                        moveEvent = $.Event(eswipemove, { delta: { x: dx, y: dy } });
                        $this.trigger(moveEvent);

                        if (moveEvent.isDefaultPrevented()) {
                            return;
                        }

                        // Measure change in x and y.
                        delta = {
                            x: dx,
                            y: dy
                        };
                    },
                    onEnd = function () {

                        // Measure duration
                        var duration = +new Date() - start.time,
                            endEvent;

                        // Determine if slide attempt triggers slide.
                        if (Math.abs(delta.x) > 1 || Math.abs(delta.y) > 1) {

                            // Set the direction and return it.
                            var horizontal = delta.x < 0 ? "left" : "right",
                                vertical = delta.y < 0 ? "up" : "down",
                                direction = Math.abs(delta.x) > Math.abs(delta.y) ? horizontal : vertical;

                            endEvent = $.Event(eswipeend, { delta: delta, direction: direction, duration: duration });

                            $this.trigger(endEvent);
                        }

                        // Disable the touch events till next time.
                        $this.off(etouch.move).off(etouch.end);
                    };

                $this.off(etouch.start).on(etouch.start, function (event) {

                    // Normalize the variables.
                    var isMouse = event.type === "mousedown",
                        isPointer = event.type !== "touchstart" && !isMouse,
                        original = event.originalEvent;

                    if ((isPointer || isMouse) && $(event.target).is("img")) {
                        event.preventDefault();
                    }

                    event.stopPropagation();

                    // Measure start values.
                    start = {
                        // Get initial touch coordinates.
                        x: isMouse ? original.pageX : isPointer ? original.clientX : original.touches[0].pageX,
                        y: isMouse ? original.pageY : isPointer ? original.clientY : original.touches[0].pageY,

                        // Store time to determine touch duration.
                        time: +new Date()
                    };

                    var startEvent = $.Event(eswipestart, { start: start });

                    $this.trigger(startEvent);

                    if (startEvent.isDefaultPrevented()) {
                        return;
                    }

                    // Reset delta and end measurements.
                    delta = { x: 0, y: 0 };

                    // Attach touchmove and touchend listeners.
                    $this.on(etouch.move, onMove)
                        .on(etouch.end, onEnd);
                });
            });
        };

        var removeSwipe = function ($elem, handler) {
            /// <summary>Removes swiping functionality from the given element.</summary>

            var ns = handler.namespace ? "." + handler.namespace : "",
                etouch = getEvents(ns);

            return $elem.each(function () {

                // Disable extended touch events on ie.
                // Unbind events.
                $(this).css({ "-ms-touch-action": "", "touch-action": "" })
                    .off(etouch.start).off(etouch.move).off(etouch.end);
            });
        };

        // Create special events so we can use on/off.
        $.event.special.swipe = {
            add: function (handler) {
                addSwipe($(this), handler);
            },
            remove: function (handler) {
                removeSwipe($(this), handler);
            }
        };
    }());

    $.extend($.expr[":"], {
        attrStart: function (el, i, props) {
            /// <summary>Custom selector extension to allow attribute starts with selection.</summary>
            /// <param name="el" type="DOM">The element to test against.</param>
            /// <param name="i" type="Number">The index of the element in the stack.</param>
            /// <param name="props" type="Object">Metadata for the element.</param>
            /// <returns type="Boolean">True if the element is a match; otherwise, false.</returns>
            var hasAttribute = false;

            $.each(el.attributes, function () {
                if (this.name.indexOf(props[3]) === 0) {
                    hasAttribute = true;
                    return false;  // Exit the iteration.
                }
                return true;
            });

            return hasAttribute;
        }
    });

    $.getDataOptions = function ($elem, filter) {
        /// <summary>Creates an object containing options populated from an elements data attributes.</summary>
        /// <param name="$elem" type="jQuery">The object representing the DOM element.</param>
        /// <param name="filter" type="String">The prefix with filter to identify the data attribute.</param>
        /// <returns type="Object">The extended object.</returns>
        var options = {};
        $.each($elem.data(), function (key, val) {
            if (key.indexOf(filter) === 0 && key.length > filter.length) {

                // Build a key with the correct format.
                var length = filter.length,
                    newKey = key.charAt(length).toLowerCase() + key.substring(length + 1);

                options[newKey] = val;
            }
        });

        return Object.keys(options).length ? options : $elem.data();
    };

    $.debounce = function (func, wait, immediate) {
        /// <summary>
        /// Returns a function, that, as long as it continues to be invoked, will not
        /// be triggered. The function will be called after it stops being called for
        /// N milliseconds. If `immediate` is passed, trigger the function on the
        /// leading edge, instead of the trailing.
        ///</summary>
        /// <param name="func" type="Function">
        ///      The function to debounce.
        /// </param>
        /// <param name="wait" type="Number">
        ///      The number of milliseconds to delay.
        /// </param>
        /// <param name="wait" type="immediate">
        ///      Specify execution on the leading edge of the timeout.
        /// </param>
        /// <returns type="jQuery">The function.</returns>
        var timeout;
        return function () {
            var context = this, args = arguments;
            w.clearTimeout(timeout);
            timeout = w.setTimeout(function () {
                timeout = null;
                if (!immediate) { func.apply(context, args); }
            }, wait);
            if (immediate && !timeout) { func.apply(context, args); }
        };
    };

    (function (old) {
        /// <summary>Override the core html method in the jQuery object to fire a domchanged event whenever it is called.</summary>
        /// <param name="old" type="Function">
        ///      The jQuery function being overridden.
        /// </param>
        /// <returns type="jQuery">The jQuery object for chaining.</returns>

        var echanged = $.Event("domchanged"),
            $d = $(d);

        $.fn.html = function () {
            // Execute the original html() method using the augmented arguments collection.
            var result = old.apply(this, arguments);

            if (arguments.length) {
                $d.trigger(echanged);
            }

            return result;

        };
    })($.fn.html);
} (jQuery, window, document));

/*global jQuery*/
/*jshint expr:true*/

(function ($, w, ns, da) {

    "use strict";

    if (w.RESPONSIVE_MODAL) {
        return;
    }

    var $window = $(w),
        $body = $("body"),
        $overlay = $("<div/>").attr({ "role": "document" }).addClass("modal-overlay modal-loader fade-out"),
        $modal = $("<div/>").addClass("modal fade-out").appendTo($overlay),
        $header = $("<div/>").addClass("modal-header fade-out"),
        $footer = $("<div/>").addClass("modal-footer fade-out"),
        $close = $("<button/>").attr({ "type": "button" }).addClass("modal-close fade-out"),
        $prev = $("<button/>").attr({ "type": "button" }).addClass("modal-direction prev fade-out"),
        $next = $("<button/>").attr({ "type": "button" }).addClass("modal-direction next fade-out"),
        $placeholder = $("<div/>").addClass("modal-placeholder"),
        // Events
        eready = "ready" + ns + da,
        echanged = "domchanged" + ns + da,
        eresize = ["resize" + ns, "orientationchange" + ns].join(" "),
        eclick = "click" + ns,
        ekeydown = "keydown" + ns,

        efocusin = "focusin" + ns,
        eshow = "show" + ns,
        eshown = "shown" + ns,
        ehide = "hide" + ns,
        ehidden = "hidden" + ns,
        eerror = "error" + ns,
        rtl = $.support.rtl,
        currentGrid = $.support.currentGrid(),
        keys = {
            ESCAPE: 27,
            LEFT: 37,
            RIGHT: 39
        },
        lastScroll = 0,
        protocol = w.location.protocol.indexOf("http") === 0 ? w.location.protocol : "http:",
        // Regular expression.
        rexternalHost = new RegExp("//" + w.location.host + "($|/)"),
        rimage = /(^data:image\/.*,)|(\.(jp(e|g|eg)|gif|png|bmp|ti(ff|f)|webp|svg)((\?|#).*)?$)/i,
        // Taken from jQuery.
        rhash = /^#.*$/, // Altered to only match beginning.
        rurl = /^([\w.+-]+:)(?:\/\/([^\/?#:]*)(?::(\d+)|)|)/,
        rlocalProtocol = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/;

    var Modal = function (element, options) {
        this.$element = $(element);
        this.defaults = {
            modal: null,
            external: false,
            group: null,
            image: false,
            immediate: false,
            iframe: false,
            iframeScroll: true,
            keyboard: true,
            touch: true,
            next: ">",
            nextHint: "Next (" + (rtl ? "Left" : "Right") + " Arrow)",
            previous: "<",
            previousHint: "Previous (" + (rtl ? "Right" : "Left") + " Arrow)",
            closeHint: "Close (Esc)",
            errorHint: "<p>An error has occured.</p>",
            mobileTarget: null,
            mobileViewportWidth: "xs",
            fitViewport: true
        };
        this.options = $.extend({}, this.defaults, options);
        this.title = null;
        this.description = null;
        this.isShown = null;
        this.$group = null;

        // Make a list of grouped modal targets.
        if (this.options.group) {
            this.$group = $(this.options.group);
        }

        // Bind events.
        // Ensure script works if loaded at the top of the page.
        if ($body.length === 0) { $body = $("body"); }
        this.$element.on(eclick, this.click.bind(this));
        var onResize = $.debounce(this.resize.bind(this), 15);
        $(w).off(eresize).on(eresize, onResize);

        if (this.options.immediate) {
            this.show();
        }
    };

    Modal.prototype.click = function (event) {

        event.preventDefault();

        // Check to see if there is a current instance running
        // so we can cater for nested triggers.
        var $current = $modal.data("currentModal");
        if ($current && $current[0] !== this.$element[0]) {
            var complete = function () {
                // Timeout > time required to fix flash error for IE9.
                w.setTimeout(function () {
                    this.show(true);
                }.bind(this), 150);
            }.bind(this);

            $current.data("r.modal").toggleModal(true, true);
            $modal.onTransitionEnd(complete);
            return;
        }

        this.show();
    };

    Modal.prototype.show = function (noOverlay) {

        if (this.isShown) {
            return;
        }

        var showEvent = $.Event(eshow);
        this.$element.trigger(showEvent);

        if (showEvent.isDefaultPrevented()) {
            return;
        }

        this.isShown = true;

        // If the trigger has a mobile target and the viewport is smaller than the mobile limit
        // then redirect to that page instead.
        if (this.options.mobileTarget) {
            var width = this.options.mobileViewportWidth;
            // Handle numeric width.
            if (typeof width === "number" && width >= $window.width()) {
                w.location.href = this.options.mobileTarget;
                return;
            }

            // Handle specific range.
            if (typeof width === "string") {
                var index = $.inArray(width, currentGrid.range);
                if (currentGrid.index <= index && index > -1) {
                    w.location.href = this.options.mobileTarget;
                    return;
                }
            }
        }

        if (!noOverlay) {
            this.overlay();
            return;
        }

        this.toggleModal();
    };

    Modal.prototype.hide = function () {

        // Destroy the modal
        this.toggleModal(true);

    };

    Modal.prototype.overlay = function () {

        // Add the overlay to the body if not done already.
        if (!$(".modal-overlay").length) {
            $body.append($overlay);
        }

        // Fade out.
        if ($overlay.hasClass("fade-in")) {

            var complete = function () {
                $modal.removeData("currentModal").removeAttr("tabindex");
                $.toggleBodyLock();
                $overlay.attr("hidden", " ");
                $window.scrollTop(lastScroll);
            }.bind(this);

            $overlay.removeClass("fade-in").onTransitionEnd(complete);
            return;
        }

        // Fade in and fire modal.
        if ($("html").attr("data-lock") === undefined) {
            lastScroll = $window.scrollTop();
            $.toggleBodyLock();
        }
        $overlay.removeAttr("hidden")
            .redraw()
            .addClass("fade-in")
            .onTransitionEnd(function () { this.toggleModal(); }.bind(this));
    };

    Modal.prototype.toggleModal = function (destroy, nested) {

        var complete;
        if (!destroy) {
            complete = function () {
                var $autofocus = $modal.find("[autofocus]");

                $body.attr({ "tabindex": -1 });
                $modal.data("currentModal", this.$element).attr({ "tabindex": 0 });
                $autofocus.length ? $autofocus.focus() : $modal.focus();

                // Ensure that focus is maintained within the modal.
                $(document).off(efocusin).on(efocusin, this.focus.bind(this));

                // Bind the keyboard and touch actions.
                if (this.options.keyboard) {
                    $(document).off(ekeydown).on(ekeydown, this.keydown.bind(this));
                }

                if (this.options.group) {
                    if (this.options.touch) {
                        $modal.off("swipe.modal").on("swipe.modal", true)
                              .off("swipeend.modal").on("swipeend.modal", this.swipeend.bind(this));
                    }
                }

                $overlay.off(eclick).on(eclick, this.overlayclick.bind(this));

                this.$element.trigger($.Event(eshown));

            }.bind(this);

            // Create the modal contents.
            this.create();
            $modal.onTransitionEnd(complete)
                  .off(eclick).on(eclick, this.modalclick.bind(this));
            return;
        }

        // We're destroying the current modal.
        if (!this.isShown) {
            return;
        }

        var hideEvent = $.Event(ehide);

        //this.$element.trigger(hideEvent); //DJS

        if (hideEvent.isDefaultPrevented()) {
            return;
        }

        this.isShown = false;

        $overlay.removeClass("modal-loader");
        $.each([$header, $footer, $close, $next, $prev, $modal], function () {

            this.removeClass("fade-in");
        });

        complete = function () {

            // Launch the next/pre grouped item.
            if (this.$sibling && this.$sibling.data("r.modal")) {
                this.destroy(true);
                this.$element.trigger($.Event(ehidden));
                // Timeout > time required to fix flash error for IE9.
                w.setTimeout(function () {
                    this.$sibling.data("r.modal").show(true);
                    this.$sibling = null;
                }.bind(this), 150);
                return;
            }

            if (nested) {
                this.destroy(true);
                this.$element.trigger($.Event(ehidden));
                return;
            }

            this.destroy();
            this.$element.trigger($.Event(ehidden));

        }.bind(this);

        // Destroy modal contents.
        $modal.onTransitionEnd(complete);
    };

    Modal.prototype.create = function () {

        $overlay.addClass("modal-loader");
        var isExternalUrl = function (url) {

            // Handle different host types.
            // Split the url into it's various parts.
            var locationParts = rurl.exec(url) || rurl.exec(protocol + url);

            if (locationParts === undefined || rhash.test(url)) {
                return false;
            }

            // Target is a local protocol.
            if (!locationParts || !locationParts[2] || rlocalProtocol.test(locationParts[1])) {
                return false;
            }

            // If the regex doesn't match return true . 
            return !rexternalHost.test(locationParts[2]);
        };

        var complete = function () {

            this.resize();

            $.each([$header, $footer, $close, $next, $prev, $modal], function () {

                this.addClass("fade-in");
            });

            $modal.redraw();

            $overlay.removeClass("modal-loader");

        }.bind(this);

        var title = this.options.title,
            description = this.options.description,
            modal = this.options.modal,
            target = this.options.target,
            notHash = !rhash.test(this.options.target),
            external = isExternalUrl(target),
            local = !notHash && !external,
            $group = this.$group,
            nextText = this.options.next + "<span class=\"visuallyhidden\">" + this.options.nextHint + "</span>",
            prevText = this.options.previous + "<span class=\"visuallyhidden\">" + this.options.previousHint + "</span>",
            iframeScroll = this.options.iframeScroll,
            image = this.options.image || rimage.test(target),
            iframe = this.options.iframe || notHash && external ? !image : false,
            $iframeWrap = $("<div/>").addClass(iframeScroll ? "media media-scroll" : "media"),
            $content = $("<div/>").addClass("modal-content");

        if ($group) {
            // Test to see if the grouped target have data.
            var $filtered = $group.filter(function () {
                return $(this).data("r.modal");
            });

            if ($filtered.length) {
                // Need to show next/prev.
                $next.html(nextText).prependTo($modal);
                $prev.html(prevText).prependTo($modal);
            }
        }

        // 1: Build the header
        if (title || !modal) {

            if (title) {
                var id = "modal-label-" + $.pseudoUnique();
                $header.html("<div class=\"container\"><h2 id=\"" + id + "\">" + title + "</h2></div>")
                       .appendTo($overlay.attr({ "aria-labelledby": id }));
            }

            if (!modal) {
                $close.html("x <span class=\"visuallyhidden\">" + this.options.closeHint + "</span>").appendTo($overlay);
            }
        }

        // 2: Build the footer
        if (description) {

            // Add footer text if necessary
            $footer.html("<div class=\"container\">" + description + "</div>")
                   .appendTo($overlay);
        }

        // 3: Build the content
        if (local) {
            var $target = $(target);
            this.isLocalHidden = $target.is(":hidden");
            $modal.addClass(this.options.fitViewport ? "container" : "");
            $placeholder.detach().insertAfter($target);
            $target.detach().appendTo($content).removeClass("hidden").attr({ "aria-hidden": false, "hidden": false });
            $content.appendTo($modal);
            // Fade in.
            complete();
        } else {
            if (iframe) {

                $modal.addClass("modal-iframe");

                // Normalize the src.
                var src = (isExternalUrl(target) && target.indexOf("http") !== 0) ? protocol + target : target,
                    getMediaProvider = function (url) {
                        var providers = {
                            youtube: /youtu(be\.com|be\.googleapis\.com|\.be)/i,
                            vimeo: /vimeo/i,
                            vine: /vine/i,
                            instagram: /instagram|instagr\.am/i,
                            getty: /embed\.gettyimages\.com/i
                        };

                        for (var p in providers) {
                            if (providers.hasOwnProperty(p) && providers[p].test(url)) {
                                return [p, "scaled"].join(" ");
                            }
                        }
                        return false;
                    };

                // Have to add inline styles for older browsers.
                $("<iframe/>").attr({
                    "scrolling": iframeScroll ? "yes" : "no",
                    "allowTransparency": true,
                    "frameborder": 0,
                    "hspace": 0,
                    "vspace": 0,
                    "webkitallowfullscreen": "",
                    "mozallowfullscreen": "",
                    "allowfullscreen": ""
                }).one("load error", function () {
                    // Fade in. Can be slow but ensures concurrency.
                    complete();
                }).appendTo($iframeWrap).attr("src", src);

                // Test and add additional media classes.
                var mediaClasses = getMediaProvider(target) || "";

                if (!mediaClasses) {
                    $modal.addClass("iframe-full");
                }

                $iframeWrap.addClass(mediaClasses).appendTo($modal);

            } else {
                if (image) {

                    $modal.addClass("modal-image");
                    $("<img/>").one("load error", function () {
                        // Fade in.
                        complete();
                    }).appendTo($modal).attr("src", target);

                } else {

                    $modal.addClass("modal-ajax");
                    $modal.addClass(this.options.fitViewport ? "container" : "");

                    // Standard ajax load.
                    $content.load(target, null, function (responseText, textStatus) {

                        if (textStatus === "error") {
                            this.$element.trigger($.Event(eerror, { relatedTarget: $content[0] }));
                            $content.html(this.options.errorHint);
                        }

                        $content.appendTo($modal);

                        // Fade in.
                        complete();
                    }.bind(this));
                }
            }
        }
    };

    Modal.prototype.destroy = function (noOverlay) {

        // Clean up the next/prev.
        $next.detach();
        $prev.detach();

        // Clean up the header/footer.
        $header.empty().detach();
        $footer.empty().detach();
        $close.detach();

        // Remove label.
        $overlay.removeAttr("aria-labelledby");

        if (!this.options.external && !$modal.is(".modal-iframe, .modal-ajax, .modal-image")) {

            // Put that kid back where it came from or so help me.
            $(this.options.target).addClass(this.isLocalHidden ? "hidden" : "")
                                  .attr({ "aria-hidden": this.isLocalHidden ? true : false, "hidden": this.isLocalHidden ? true : false })
                                  .detach().insertAfter($placeholder);

            $placeholder.detach().insertAfter($overlay);
        }

        // Fix __flash__removeCallback' is undefined error in IE9.
        $modal.find("iframe").attr("src", "");
        w.setTimeout(function () {
            $modal.removeClass("modal-iframe iframe-full modal-ajax modal-image container").css({
                "max-height": "",
                "max-width": ""
            }).empty();

        }.bind(this), 100);

        if (!noOverlay) { this.overlay(); }
    };

    Modal.prototype.overlayclick = function (event) {

        if (this.options.modal) {
            return;
        }

        var closeTarget = $close[0],
            eventTarget = event.target;

        if (eventTarget === $modal[0] || $.contains($modal[0], eventTarget)) {
            return;
        }

// The ability to close the modal via the cross // DJS
        if (eventTarget === closeTarget) {
            this.hide();
            return;
        }
        
// The ability to close the modal via the screen // DJS
        if (eventTarget === $overlay[0] || ($.contains($overlay[0], eventTarget))) {
            this.hide();
        }
    };

    Modal.prototype.modalclick = function (event) {

        var next = $next[0],
            prev = $prev[0],
            eventTarget = event.target;

        if (eventTarget === next || eventTarget === prev) {
            event.preventDefault();
            event.stopPropagation();
            this[eventTarget === next ? "next" : "prev"]();
            return;
        }

        if (this.options.modal) {
            if (eventTarget === $modal.find(this.options.modal)[0]) {
                event.preventDefault();
                event.stopPropagation();
                this.hide();
                //console.log('1');
                //Wasn't this one // DJS
            }
        }
    };

    Modal.prototype.focus = function (event) {

        if (event.target !== $overlay[0] && !$.contains($overlay[0], event.target)) {
            var $newTarget = $modal.find("a, area, button, input, object, select, textarea, [tabindex]").first();
            $newTarget.length ? $newTarget.focus() : $modal.focus();

            return false;
        }
        return true;
    };

    Modal.prototype.keydown = function (event) {

        if (this.options.modal) {
            return;
        }

        // Bind the escape key.
        if (event.which === keys.ESCAPE) {
// The ability to close the modal via the ESC key // DJS
            this.hide();
            return;
        }

        // Bind the next/prev keys.
        if (this.options.group) {

            if (/input|textarea/i.test(event.target.tagName)) {
                return;
            }

            // Bind the left arrow key.
            if (event.which === keys.LEFT) {
                rtl ? this.next() : this.prev();
            }

            // Bind the right arrow key.
            if (event.which === keys.RIGHT) {
                rtl ? this.prev() : this.next();
            }
        }
    };

    Modal.prototype.swipeend = function (event) {
        if (rtl) {
            this[(event.direction === "right") ? "prev" : "next"]();
            return;
        }

        this[(event.direction === "right") ? "next" : "prev"]();
    };

    Modal.prototype.resize = function () {

        // Resize the modal
        var windowHeight = $window.height(),
            headerHeight = $header.length && $header.height() || 0,
            closeHeight = $close.length && $close.outerHeight() || 0,
            topHeight = closeHeight > headerHeight ? closeHeight : headerHeight,
            footerHeight = $footer.length && $footer.height() || 0,
            maxHeight = (windowHeight - (topHeight + footerHeight)) * 0.95;

        $(".modal-overlay").css({ "padding-top": topHeight, "padding-bottom": footerHeight });

        if ($modal.hasClass("modal-image")) {

            $modal.children("img").css("max-height", maxHeight);
        } else if ($modal.hasClass("modal-iframe")) {

            // Calculate the ratio.
            var $iframe = $modal.find(".media > iframe"),
                iframeWidth = $iframe.width(),
                iframeHeight = $iframe.height(),
                ratio = iframeWidth / iframeHeight,
                maxWidth = maxHeight * ratio;

            // Set both to ensure there is no overflow.
            if ($iframe.parent().hasClass("scaled")) {
                $modal.css({
                    "max-height": maxHeight,
                    "max-width": maxWidth
                });
            }

        } else {
            var $content = $modal.children(".modal-content");

            $.each([$modal, $content], function () {
                this.css({
                    "max-height": maxHeight
                });
            });
        }

        // Reassign the current grid.
        currentGrid = $.support.currentGrid();
    };

    Modal.prototype.direction = function (course) {

        if (!this.isShown) {
            return;
        }

        if (this.options.group) {
            var index = this.$group.index(this.$element),
                length = this.$group.length,
                position = course === "next" ? index + 1 : index - 1;

            if (course === "next") {

                if (position >= length || position < 0) {

                    position = 0;
                }
            } else {

                if (position >= length) {

                    position = 0;
                }

                if (position < 0) {
                    position = length - 1;
                }
            }

            // Assign the sibling and destroy the current modal.
            this.$sibling = $(this.$group[position]);
            this.hide();
            //console.log('2');
            //Wasn't this one // DJS
        }
    };


    Modal.prototype.next = function () {
        this.direction("next");
    };

    Modal.prototype.prev = function () {
        this.direction("prev");
    };

    // No conflict.
    var old = $.fn.modal;

    // Plug-in definition 
    $.fn.modal = function (options) {

        return this.each(function () {
            var $this = $(this),
                data = $this.data("r.modal"),
                opts = typeof options === "object" ? $.extend({}, options) : {};

            if (!opts.target) {
                opts.target = $this.attr("href");
            }

            if (!data) {
                // Check the data and reassign if not present.
                $this.data("r.modal", (data = new Modal(this, opts)));
            }

            // Run the appropriate function if a string is passed.
            if (typeof options === "string" && /(show|hide|next|prev)/.test(options)) {
                data[options]();
            }
        });
    };

    // Set the public constructor.
    $.fn.modal.Constructor = Modal;

    $.fn.modal.noConflict = function () {
        $.fn.modal = old;
        return this;
    };

    // Data API
    var init = function () {
        $(":attrStart(data-modal)").each(function () {
            var $this = $(this),
                loaded = $this.data("r.modalLoaded");
            if (!loaded) {
                $this.data("r.modalLoaded", true);
                $this.modal($.getDataOptions($this, "modal"));
            }
        });
    },
    debouncedInit = $.debounce(init, 500);

    $(document).on([eready, echanged, eshown].join(" "), function (event) {
        event.type === "ready" ? init() : debouncedInit();
    });

    w.RESPONSIVE_MODAL = true;

}(jQuery, window, ".r.modal", ".data-api"));