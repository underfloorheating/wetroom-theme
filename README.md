# Wetroom Shower Trays Theme #

Theme includes:
- A Theme module with helper config.
- Transactional Email templates.
- Javascript Translations - add translations to Javascript using:
  1. Translator.translate('text to translate here')
  2. Add the translation in the skin locale translate.csv file
  3. Add the translation to the theme app/code/local/Redbox/Theme/etc/jstranslator.xml file - see example to follow.
- Custom 503 error page - this will be displayed when the site goes down due to a 503 error and when a code deployment is being performed.
- Config allows upload of .pdf & .css files to admin CMS WYSISYG folders.
- Full width & empty page (ajax) design layout options.
- Transactional email tempaltes to add product images

+ Sass & Javascript functions & plugins. 
Includes:
- Foundation Grid mixins for allowing responsive grids using classes in the CMS wysiwyg
- Custom base styles to help start off project quickly
- Owl carousel
- Additional mixins & helpers - incluing button, css accordion, cms footer
- Retina sprites & Redbox font icon (see http://wayneholland.co.uk/sites/axa/icons.html) 


You can make a copy of this to use for your project theme. After git cloning the theme into a test area:
- Copy complete folder
- rm -rf .git
- Change composer.json
- Change theme & app folder names (e.e app/design/frontend/redbox -> app/design/frontend/project-name
- Change folder name inside app/design/frontend/redbox/default/layout/redbox
- Change reference at the bottom of app/design/frontend/redbox/default/etc/theme.xml
