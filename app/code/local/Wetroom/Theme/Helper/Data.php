<?php
/**
 * Main helper class
 *
 * @category  Translate
 * @package   Wetroom_theme
 */
class Wetroom_Theme_Helper_Data extends Mage_Core_Helper_Abstract {

	/*
	 * Find out the highest discount value from category
	 * @return int
	 * @author Fei Chen fei.chen@redboxdigital.com
	 */
	public function maxCategoryDiscount($cat)
	{
		$toCompare = array ();
		$categoryId = $cat->getId ();
		$readAdapter = Mage::getSingleton ( 'core/resource' )->getConnection ( 'core_read' );
		$storeId = Mage::app ()->getStore ()->getStoreId ();
		$select = $readAdapter->select ();
		$columns = array (
				'percentage' => '(product.price - product.special_price)/product.price*100'
		);
		$select->from ( array (
				'product' => $this->_getTable ( 'catalog/product_flat' ) . '_' . $storeId
		), $columns )->joinLeft ( array (
				'cp' => $this->_getTable ( 'catalog/category_product' )
		), 'product.entity_id = cp.product_id', array () )->where ( 'cp.category_id = "' . $categoryId . '" AND product.special_price IS NOT NULL' )->order ( 'percentage DESC' )->limit ( '1' );
		$result = $readAdapter->fetchRow ( $select );
		if ($result ['percentage']) {
			$discountNotLoggedIn = $result ['percentage'];
			array_push ( $toCompare, $discountNotLoggedIn );
		} else {
			array_push ( $toCompare, 0 );
		}

		$roleId = Mage::getSingleton ( 'customer/session' )->getCustomerGroupId();

		if ($roleId > 0) {

			$select = $readAdapter->select ();
			$columns = array (
					'percentage' => '(product.price - price.value)/product.price*100'
			);
			$select->from ( array (
					'product' => $this->_getTable ( 'catalog/product_flat' ) . '_' . $storeId
			), $columns )->joinLeft ( array (
					'price' => $this->_getTable ( 'catalog/product_attribute_group_price' )
			), 'price.entity_id = product.entity_id', array () )->joinLeft ( array (
					'cp' => $this->_getTable ( 'catalog/category_product' )
			), 'product.entity_id = cp.product_id', array () )->where ( 'cp.category_id = "' . $categoryId . '" AND price.value IS NOT NULL AND price.customer_group_id = "' . $roleId . '"' )->order ( 'percentage DESC' )->limit ( '1' );

			$result = $readAdapter->fetchRow ( $select );
			if ($result ['percentage']) {
				$discountLoggedIn = $result ['percentage'];
				array_push ( $toCompare, $discountLoggedIn );
			} else {
				array_push ( $toCompare, 0 );
			}

			$select = $readAdapter->select ();
			$columns = array (
					'price' => 'product.price',
					'group_price' => 'price.value',
					'percentage' => '(product.price - price.value)/product.price*100'
			);
			$select->from ( array (
					'link' => $this->_getTable ( 'catalog/product_super_link' )
			), $columns )->joinLeft ( array (
					'cp' => $this->_getTable ( 'catalog/category_product' )
			), 'link.parent_id = cp.product_id', array () )->joinLeft ( array (
					'product' => $this->_getTable ( 'catalog/product_flat' ) . '_' . $storeId
			), 'cp.product_id = product.entity_id', array () )->joinLeft ( array (
					'price' => $this->_getTable ( 'catalog/product_attribute_group_price' )
			), 'price.entity_id = link.product_id', array () )->where ( 'cp.category_id = "' . $categoryId . '" AND price.value IS NOT NULL AND price.customer_group_id = "' . $roleId . '"' )->order ( 'percentage DESC' )->limit ( '1' );

			$result = $readAdapter->fetchRow ( $select );
			if ($result ['percentage']) {
				$discountLoggedInSimpleConf = $result ['percentage'];
				array_push ( $toCompare, $discountLoggedInSimpleConf );
			} else {
				array_push ( $toCompare, 0 );
			}
		}

		$highestDiscount = max ( $toCompare );

		return round ( $highestDiscount, 0 );
	}

	/*
	 * Find out the lowerst price from category
	 * @return int
	 * @author Fei Chen fei.chen@redboxdigital.com
	 */
	public function minCategoryPrice($cat) {
		$toCompare = array ();
		$productColl = Mage::getModel ( 'catalog/product' )->getCollection ()->addCategoryFilter ( $cat )->addAttributeToSort ( 'price', 'asc' )->setPageSize ( 1 )->load ();

		$lowestProductPrice = $productColl->getFirstItem ()->getMinPrice ();

		array_push ( $toCompare, $lowestProductPrice );

		$productColl = Mage::getModel ( 'catalog/product' )->getCollection ()->addCategoryFilter ( $cat )->addAttributeToFilter ( 'special_price', array (
				'notnull' => true
		) )->addAttributeToSort ( 'special_price', 'asc' )->setPageSize ( 1 )->load ();
		$lowestProductSpecialPrice = $productColl->getFirstItem ()->getSpecialPrice ();

		if ($lowestProductSpecialPrice) {
			array_push ( $toCompare, $lowestProductSpecialPrice );
		}
		$roleId = Mage::getSingleton ( 'customer/session' )->getCustomerGroupId ();
		if ($roleId > 0) {
			$categoryId = $cat->getId ();
			$readAdapter = Mage::getSingleton ( 'core/resource' )->getConnection ( 'core_read' );

			// check minimum price from simple products under configurable
			$select = $readAdapter->select ();
			$columns = array (
					'value' => 'price.value'
			);
			$select->from ( array (
					'link' => $this->_getTable ( 'catalog/product_super_link' )
			), $columns )->joinLeft ( array (
					'cp' => $this->_getTable ( 'catalog/category_product' )
			), 'link.parent_id = cp.product_id', array () )->joinLeft ( array (
					'price' => $this->_getTable ( 'catalog/product_attribute_group_price' )
			), 'price.entity_id = link.product_id', array () )->where ( 'cp.category_id = "' . $categoryId . '" AND price.value IS NOT NULL AND price.customer_group_id = "' . $roleId . '"' )->order ( 'price.value ASC' )->limit ( '1' );

			$result = $readAdapter->fetchRow ( $select );
			if ($result ['value']) {
				$minimumPriceConSimple = $result ['value'];
				array_push ( $toCompare, $minimumPriceConSimple );
			}
			// check minimum price for simple products

			$select = $readAdapter->select ();
			$columns = array (
					'value' => 'price.value'
			);
			$select->from ( array (
					'cp' => $this->_getTable ( 'catalog/category_product' )
			), $columns )->joinLeft ( array (
					'price' => $this->_getTable ( 'catalog/product_attribute_group_price' )
			), 'price.entity_id = cp.product_id', array () )->where ( 'cp.category_id = "' . $categoryId . '" AND price.value IS NOT NULL AND price.customer_group_id = "' . $roleId . '"' )->order ( 'price.value ASC' )->limit ( '1' );
			$result = $readAdapter->fetchRow ( $select );

			if ($result ['value']) {
				$minimumPriceSimple = $result ['value'];
				array_push ( $toCompare, $minimumPriceSimple );
			}
		}
		//find lowest from all senarios
		$lowestProductPrice = min ( $toCompare );

		return number_format ( $lowestProductPrice, 2 );
	}


	/*
	 * Find table name
	 * @return string
	 * @author Fei Chen fei.chen@redboxdigital.com
	 */
	protected function _getTable($table)
	{
		return Mage::getSingleton ( 'core/resource' )->getTableName ( $table );
	}

	/*
	 * Return customer group name for top.links
	 * @return string
	 * @author Dominic Sutton dominic.sutton@theunderfloorheatingstore.com
	 */
	public function customerGroup()
	{
        $groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        $group = Mage::getModel('customer/group')->load($groupId);
        return 'My '.$group->getCode().' Account';
    }

    public function customerGroupImage()
    {
    	$groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();
        $group = Mage::getModel('customer/group')->load($groupId);
        $code = trim(str_replace(' ','',$group->getCode()));
        if($groupId == 5 || $groupId == 4)
        {
        	$file = '/media/wysiwyg/dashboard/' . strtolower($code) . '.jpg';
        	if(file_exists(Mage::getBaseDir('base') . $file)) {
	        	return '<img id="customer-group-dashboard-image" src="' . $file . '"/>';
	        }
        }
        return false;
    }
}